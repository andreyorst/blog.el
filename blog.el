;;; blog.el --- Automating my blog -*- lexical-binding: t -*-
;;
;; Author: Andrey Listopadov
;; Homepage: https://gitlab.com/andreyorst/blog.el
;; Package-Requires: ((emacs "28.1") (ox-hugo))
;; Keywords: indent lisp tools
;; Prefix: blog
;; Version: 0.0.2
;;
;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with isayt.  If not, see <http://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; I use this package to manage my blog at https://andreyor.st, but it
;; has almost nothing specific to my blog, so it can be used for any
;; other blog.  The only hard requirement is that the blog is managed
;; with the ox-hugo.
;;
;; This package is highly opinionated and was part of my configuration
;; before I extracted it to a package, so any suggestions for
;; generalizing it or making it work with other blogging
;; engines/packages will be probably turned down.  It works for me,
;; and I'm the main user, so my needs have higher priority.  But, as
;; this is FOSS, if you need something this package doesn't provide
;; yet, you're free to use it as is, or to fork and adjust for your
;; specific workflow.  Improvements and bug fixes for the existing
;; functionality are always welcome though.
;;
;;; Code:

(require 'subr-x)
(declare-function org-hugo-export-to-md "ext:ox-hugo")

(defvar blog-current-post-name nil
  "This variable is used to store current post name.
Do not set it manually.")

(defcustom blog-capture-template-string
  "#+hugo_base_dir: ../
#+hugo_section: posts
#+hugo_auto_set_lastmod: t
#+options: tex:dvisvgm

#+title: %(blog-current-post-name)
#+date: %(format-time-string \"%Y-%m-%d %h %H:%M\")
#+hugo_tags: %(blog-read-tags)
#+hugo_categories: %(blog-read-categories)
#+hugo_custom_front_matter: :license \"%(blog-license)\"
#+hugo_draft: true

%?"
  "Capture template string."
  :type 'string
  :group 'blog)

(defcustom blog-capture-template
  `("p" "Post" plain
    (function blog--generate-file-name)
    ,blog-capture-template-string
    :jump-to-captured t
    :immediate-finish t)
  "Org-capture template for blog posts."
  :type 'sexp
  :group 'blog)

(defcustom blog-directory "~/blog"
  "Location of the blog directory for `org-capture'."
  :type 'string
  :group 'blog)

(defcustom blog-tags-file nil
  "A file with a list of tags used for posts.

A value of NIL means that the filename is constructed
automatically from the `blog-directory' variable.  A string value
can be used to specify a path.

Automatic name is constructed via:

\(expand-file-name \".tags\" blog-directory)"
  :type '(choice
          (const :tag "Automatic" nil)
          (string :tag "File"))
  :group 'blog
  :package-version '(fennel-mode "0.0.2"))

(defcustom blog-categories-file nil
  "A file with a list of tags used for posts.

A value of NIL means that the filename is constructed
automatically from the `blog-directory' variable.  A string value
can be used to specify a path.

Automatic name is constructed via:

\(expand-file-name \".categories\" blog-directory)"
  :type '(choice
          (const :tag "Automatic" nil)
          (string :tag "File"))
  :group 'blog
  :package-version '(fennel-mode "0.0.2"))

(defcustom blog-license-file nil
  "File containing license for post frontmatter.

A value of NIL means that the filename is constructed
automatically from the `blog-directory' variable.  A string value
can be used to specify a path.

Automatic name is constructed via:

\(expand-file-name \".license\" blog-directory)"
  :type '(choice
          (const :tag "Automatic" nil)
          (string :tag "File"))
  :group 'blog
  :package-version '(fennel-mode "0.0.2"))

(defun blog-license ()
  "Return the license string for post frontmatter."
  (with-temp-buffer
    (let ((file (expand-file-name
                 (or blog-license-file
                     (expand-file-name ".license" blog-directory)))))
      (when (file-exists-p  file)
        (insert-file-contents file)))
    (string-trim (buffer-substring-no-properties (point-min) (point-max)))))

(defun blog--read-file (file)
  "Read, as in `read' from the given FILE."
  (with-temp-buffer
    (let ((file (expand-file-name file)))
      (when (file-exists-p file)
        (insert-file-contents file)))
    (condition-case nil
        (read (current-buffer))
      (end-of-file nil))))

(defun blog--write-to-file (file items)
  "Write the FILE appending the ITEMS to the list in the given file."
  (let ((data (blog--read-file file)))
    (dolist (item items)
      (unless (member item data)
        (setq data (cons item data))))
    (with-temp-buffer
      (switch-to-buffer (current-buffer))
      (insert ";; -*- mode: lisp-data -*-\n")
      (insert ";; this file is automatically created, be careful when editing manually\n\n")
      (pp (sort data #'string<) (current-buffer))
      (write-region nil nil (expand-file-name file)))))

(defun blog--read-list-items (prompt file)
  "Completing read items with the PROMPT from the FILE.

FILE is a path to the file, which contains a single Lisp list literal, which
items are read by the `completing-read' function.
New items, are appended to the FILE automatically."
  (let ((items (blog--read-file file)) item result)
    (while (not (string-empty-p item))
      (setq item (string-trim (or (completing-read prompt items) "")))
      (unless (string-empty-p item)
        (push item result)
        (setq items (remove item items))))
    (blog--write-to-file file result)
    (string-join result " ")))

(defun blog-read-tags ()
  "Read the tags file for the blog."
  (blog--read-list-items
   "Select tags: "
   (or blog-tags-file
       (expand-file-name ".tags" blog-directory))))

(defun blog-read-categories ()
  "Read the categories file for the blog."
  (blog--read-list-items
   "Select categories: "
   (or blog-categories-file
       (expand-file-name ".categories" blog-directory))))

(defun blog--title-to-fname (title)
  "Format the TITLE string as a file name."
  (thread-last
    title
    (replace-regexp-in-string "[[:space:]]" "-")
    (replace-regexp-in-string "-+" "-")
    (replace-regexp-in-string "[^[:alnum:]-]+" "")
    downcase))

(defun blog--generate-file-name (&rest _)
  "Generate filename during capturing process."
  (let ((title (read-string "Title: ")))
    (setq blog-current-post-name title)
    (find-file
     (file-name-concat
      (expand-file-name blog-directory)
      "posts"
      (format "DRAFT-%s-%s.org"
              (format-time-string "%Y-%m-%d")
              (blog--title-to-fname title))))))

(defun blog-current-post-name ()
  "Return the name of the current post during template expansion.

Don't use this as a general way to obtain the name of the current
post - it only works during the expansion of the Org template."
  (format "%s" blog-current-post-name))

;;;###autoload
(defun blog-publish-file ()
  "Update '#+date:' tag, and rename the currently visited file.
File name is updated to include the same date and current title.
Export the file to md with the `ox-hugo' package.  Non-drafts are
not published."
  (interactive)
  (let ((old-name (file-name-base (or (buffer-file-name)
                                      (user-error "Buffer is not associated with any file")))))
    (if (not (save-excursion
               (goto-char (point-min))
               (re-search-forward "^#\\+hugo_draft: true$" nil 'no-error)))
        (user-error "The '%s' is already published" old-name)
      (save-match-data
        (let* ((today (format-time-string "%Y-%m-%d"))
               (now (format-time-string "%h %H:%M"))
               (file-name (save-excursion
                            (goto-char (point-min))
                            (re-search-forward "^#\\+title:[[:space:]]*\\(.*\\)$")
                            (blog--title-to-fname (match-string 1))))
               (new-name (format "%s-%s" today
                                 (if (string-match
                                      "^[[:digit:]]\\{4\\}-[[:digit:]]\\{2\\}-[[:digit:]]\\{2\\}-\\(.*\\)$"
                                      file-name)
                                     (match-string 1 file-name)
                                   file-name))))
          (save-excursion
            (goto-char (point-min))
            (re-search-forward "^#\\+date:.*$")
            (replace-match (format "#+date: %s %s" today now))
            (re-search-forward "^#\\+hugo_draft:.*$")
            (replace-match "#+hugo_draft: false"))
          (save-excursion
            (while (re-search-forward
                    (format "\\(\\[\\[.*:.*static/\\)%s\\(.*\\]\\]\\)"
                            (regexp-quote old-name))
                    nil t)
              (replace-match (format "\\1%s\\2" new-name))))
          (save-buffer)
          (let* ((exported-file (org-hugo-export-to-md))
                 (static-dir (expand-file-name "static" blog-directory)))
            (condition-case nil
                (rename-visited-file (format "%s.org" new-name))
              (file-already-exists nil))
            (rename-file
             exported-file
             (expand-file-name
              (file-name-with-extension new-name "md")
              (file-name-directory exported-file))
             t)
            (when (file-exists-p (expand-file-name old-name static-dir))
              (condition-case nil
                  (rename-file (expand-file-name old-name static-dir)
                               (expand-file-name new-name static-dir))
                (file-already-exists nil)))
            (org-hugo-export-to-md)))))))

;;;###autoload
(defun blog-scour-svg (file)
  "Process an SVG FILE with the `scour' program to reduce its size."
  (if (executable-find "scour")
      (let ((tmp (make-temp-file "scour.svg.")))
        (with-temp-file tmp
          (insert-file-contents-literally file))
        (make-process :name "scour"
                      :connection-type 'pipe
                      :sentinel (lambda (_ _) (delete-file tmp))
                      :command (list "scour" "-i" tmp "-o" file)))
    (user-error "Scour is not installed")))

;;;###autoload
(defun blog-export-static-org-link (path description _backend _properties)
  "Export link to an Org file in the static directory.
Instead of referring to it as a post, which is the default
behavior for `ox-hugo', this function forces the export to
produce a plain markdown link to a file.

PATH is used as the link target, DESCRIPTION is used as link
text.

It can be set up as:

\(org-link-set-parameters
 \"org\"
 :export #\='blog-export-static-org-link
 :complete #\='blog-create-static-org-link)"
  (defvar org-hugo-base-dir)
  (let ((new-path (expand-file-name
                   (file-name-concat "../static" path)
                   org-hugo-base-dir)))
    (copy-file (expand-file-name path) new-path t)
    (format "[%s](/%s)"
            (or description path)
            (file-name-nondirectory path))))

;;;###autoload
(defun blog-create-static-org-link (&optional _)
  "Create a file link using completion.
See `blog-export-static-org-link' for more info."
  (let ((file (read-file-name "File: "))
	(pwd (file-name-as-directory (expand-file-name ".")))
	(pwd1 (file-name-as-directory (abbreviate-file-name
				       (expand-file-name ".")))))
    (cond ((string-match
	    (concat "^" (regexp-quote pwd1) "\\(.+\\)") file)
	   (concat "org:" (match-string 1 file)))
	  ((string-match
	    (concat "^" (regexp-quote pwd) "\\(.+\\)")
	    (expand-file-name file))
	   (concat "org:"
		   (match-string 1 (expand-file-name file))))
	  (t (concat "org:" file)))))

;;;###autoload
(defun blog-create-static-dir-for-post ()
  "Create a sub directory in the static directory named as the post file."
  (interactive)
  (thread-first
    (file-name-concat
     blog-directory "static" (file-name-base (buffer-file-name)))
    expand-file-name
    (make-directory 'create-parents)))

(provide 'blog)
;;; blog.el ends here
